import React, { Component } from 'react';
// import firebase from '../../../node_modules/firebase';
import { Grid, Snackbar, Fade } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {Button, Dialog, DialogContent,DialogActions, TextField} from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import Updates from './Updates';
// import { DB_CONFIG } from '../../Assest/Config';
import {updatesExamples} from '../../Assest/ExamplesData';

/* Overriding CSS */
const styles = theme => ({
  root: {
    color: 'grey',
    primary: 'grey',
    secondery: 'grey',
  },
  btn: {
    position: 'absolute',
    right: '25px',
    bottom: '84%',
    color: 'grey',
    primary: 'grey',

  },
  textfield: {
    width: '500px',
    color: '#009fd9',
    primary: '#009fd9',
    secondery: '#009fd9',
    inputFocused: {
      borderColor: 'grey'
    }
  },
  subBtn: {
    color: '#009fd9',
    primary: '#009fd9',
  },
  btnPadding: {
    marginBottom: '10px',
    marginTop: '5px',
  },
  successSnack: {
    backgroundColor: green[600],
  },
  faildSnack: {
    backgroundColor: theme.palette.error.dark,
  }
})

class Recents extends Component {

  constructor (props){
    super (props);
    this.state = {
      updates: [],
      newUpdates: [],
      oldUpdates: [],
      text: '',
      active: true,
      open: false, 
      isEdit: false,
      editedCardIndex: '',
      all: props.all,
      successSnack: false,
      failSnack: false,
      message: ''
    }

    this.textChangeListener = this.textChangeListener.bind(this);
    this.submitHandller = this.submitHandller.bind(this);
    this.editLisenter = this.editLisenter.bind(this);
    this.editSubmitHandller = this.editSubmitHandller.bind(this);
    this.getUpdates = this.getUpdates.bind(this);
  }

  /* Fetching records from Firebase */
  componentDidMount (){
    var _newUpdates = updatesExamples;
    this.setState({updates: _newUpdates});
  }

  /* Handelling the Confirm/Add click */
  submitHandller = (event) => {
    const update = {
      text: this.state.text, 
      time: this.generateDate(), 
      isActive: true
    }
    if (update.text===undefined || update.text===''){
      this.setState({failSnack: true, message: "Missing input"});
    }
    else {
      let updates = this.state.updates;
      updates.unshift(update);
      // instead of manupulating the array use getUpdates
      this.setState({
        updates: this.getUpdates(),
        open: false,
        successSnack: true,
        message: "Added new update successfully"
      });
    }
  }

  /* Removing an update */
  archiveLisenter = (cardIndex) => {
    var _updates = [...this.state.updates];
    _updates.splice(cardIndex,1);
    this.setState({updates:_updates, successSnack: true, message: "Update archived successfully"});
  }

  /* Applying existing data to state */
  editLisenter = (cardIndex) => {
    const updates = [...this.state.updates];
    this.setState({open: true, text: updates[cardIndex].text, isEdit: true, editedCardIndex: cardIndex});
  }

  /* Updating existing data */
  editSubmitHandller = (event) => {
    const _updates = [...this.state.updates];
    const cardIndex = this.state.editedCardIndex;
    const finalText = this.state.text;
    if (finalText===undefined || finalText===''){
      this.setState({failSnack: true, message: "Missing input"});
    }
    else {
      _updates[cardIndex].text=finalText;
      _updates[cardIndex].time=this.generateDate();
      this.setState({open:false,editedCardIndex:'',updates:_updates, text:'', isEdit:false, successSnack: true, message: "Edited update successfully"}); 
    }
  }

  /* Open Modal */
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  /* Close Modal */
  handleClose = () => {
    this.setState({ open: false , text: '', isEdit: false});
  };

  /* Listen for input change */
  textChangeListener = (event) => {
    this.setState({text: event.target.value});
  }

  /* Generate new Date */
  generateDate() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    if (hours<10) {
      hours = '0' + hours.toString();
    }
    if (minutes<10) {
      minutes = '0' + minutes.toString();
    }
    var newTime =  (date.getDate() + '/' + Number(date.getMonth()+1)+ '/' + date.getFullYear() + '\n' + hours + ':' + minutes);
    return newTime;
  }

  /* Return last 10 or all updates */
  getUpdates () {
    var _updates = this.state.updates;
    var _min_updates = _updates.slice(0,9); 
    var getAll = this.state.all;
    if (getAll) {
      return _updates;
    }
    else  {
      return _min_updates;
    }
  }

   /* Closing snackbar */
   handleSnackClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ failSnack: false, successSnack: false, message:'' });
}

  render() {
    const {classes} = this.props;
    return (
      <div className="recentsDiv">
        <Grid container direction="row">
          <Grid item className={classes.btnPadding}> <Button className={classes.subBtn} size="small" variant="outlined"  onClick={this.handleClickOpen}> Add new update </Button> </Grid>
            </Grid>
              <Updates
                updates = {this.getUpdates()}
                archiveClick={this.archiveLisenter} 
                editLisenter={this.editLisenter}/>
                <Grid container spacing={8}>
                <Grid item xs>
                    <Dialog
                    className={classes.root}
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="draggable-dialog-title"
                    >
                    <DialogContent>
                    <TextField
                        id="outlined-textarea"
                        defaultValue={this.state.isEdit ? this.state.text : ''}
                        multiline
                        margin="normal"
                        variant="outlined"
                        fullWidth
                        placeholder="Insert your update here..."
                        className={classes.textfield}
                        onChange={this.textChangeListener}
                    />
                    </DialogContent>
                    <DialogActions>
                        <Button className={classes.subBtn} onClick={this.handleClose} color="primary"> Cancel </Button>
                        <Button className={classes.subBtn} onClick={this.state.isEdit ? this.editSubmitHandller : this.submitHandller} color="primary"> {this.state.isEdit ? "Confirm" : "Add"} </Button>
                    </DialogActions>
                    </Dialog>
            </Grid>
          </Grid>
          <Snackbar
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={this.state.successSnack}
                autoHideDuration={3000}
                onClose={this.handleSnackClose}
                ContentProps={{
                  'aria-describedby': 'message-id',
                  className: classes.successSnack
                }}
                TransitionComponent={Fade}
                message={this.state.message}
              />
              <Snackbar
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={this.state.failSnack}
                autoHideDuration={3000}
                onClose={this.handleSnackClose}
                ContentProps={{
                  'aria-describedby': 'message-id',
                  className: classes.faildSnack
                }}
                TransitionComponent={Fade}
                message={this.state.message}
              />
      </div>
    );
  }
}

export default withStyles(styles)(Recents);

