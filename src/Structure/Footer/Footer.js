import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';

const styles = theme => ({
    root: {
        bottom: '0',
        paddingLeft: '30%',
        zIndex: 'auto',
        width: '100%',
        height: '60px', 
        position: 'fixed',
        display: 'flex',
        color: 'grey',
        flexDirection: 'column'
    }
  })

function Footer (props)  {
    const {classes} = props;
    return (
        <div>
            <BottomNavigation className={classes.root}> 
                    <p> Created and Designed by Oz Avraham </p>
            </BottomNavigation>
        </div>
        
    );
}

export default withStyles(styles)(Footer);
