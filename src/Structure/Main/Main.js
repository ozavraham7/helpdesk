import React, {Fragment} from 'react';
import { BrowserRouter, Switch, Route} from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import BugsTracker from '../../Components/BugsTracker/BugsTracker'
import HomePage from '../../Components/HomePage/HomePage';
import SideNav from '../SideNav/SideNav';
import RecentUpdates from '../../Components/RecentUpdates/RecentUpdates';
import PageTemplate from '../../Components/PageTemplate/PageTemplate';
import FrequentAQ from '../../Components/FrequentAQ/FrequentAQ';


const styles = theme => ({
  root: {
    paddingTop: '70px',
    paddingLeft: '210px',
    position: 'flex',
    height: '100%',
    width: '-webkit-fill-available',
    alignContent: 'left',
    display: 'inline-block',
    textAlign: 'left'
  },
  content: {
    
  },
  toolbar: theme.mixins.toolbar,
});


const Main = (props) =>  {

  const {classes} = props;

    return (
      <BrowserRouter>
        <Switch>
          <Fragment>
          <Grid className={classes.root}>
            <SideNav/> 
            <Route exact path="/" component={HomePage}/>
            <Route path="/bugstracker" component={BugsTracker}/>
            <Route path="/recentupdates" component={RecentUpdates}/>
            <Route path="/frequentaq" component={FrequentAQ}/>
            <Route path="/pagetemplate" component={PageTemplate}/>
            </Grid>
          </Fragment>
        </Switch>
      </BrowserRouter> );
  
}
     
Main.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Main);