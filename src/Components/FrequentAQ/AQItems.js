import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import  {Button} from '@material-ui/core';

const styles = theme => ({
    root: {
      width: '100%',
    },
    headingQ: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
      color: 'red'
    },
    headingA: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
        color: 'green'
      },
    submmited: {
        fontWeight: 'lighter',
        paddingLeft: '5px',
    },
    content : {
        paddingLeft: '5px',
    },
    subBtn: {
        color: '#009fd9',
        primary: '#009fd9',
      },
  });

const AQItems = (props) => {
    const {classes} = props;
    return props.data.map((_item, index) => {
        return (
            <div key={_item.id}>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                        <Typography className={classes.headingQ}>Question: </Typography>
                        <Typography className={classes.content}>{" " + _item.question + " "} </Typography>
                        <Typography className={classes.submmited}> submitted on {_item.time}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails> 
                        <Typography className={classes.headingA}>Answer:</Typography>
                        <Typography className={classes.content}>{_item.answer}</Typography>
                    </ExpansionPanelDetails>
                    <ExpansionPanelActions>
                        <Button className={classes.subBtn} size="small" onClick= {() => props.editSubmitListener(index)}> Edit </Button>
                        <Button className={classes.subBtn} size="small" onClick= {() => props.deleteLisenter(index)}> Delete </Button>
                    </ExpansionPanelActions>
                </ExpansionPanel>
            </div>
        )
    })
}

PropTypes.QAItems = {
    
}

export default withStyles(styles)(AQItems);