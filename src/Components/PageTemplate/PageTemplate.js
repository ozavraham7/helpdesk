import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Footer from '../../Structure/Footer/Footer';
import { Typography } from '@material-ui/core';



const styles = theme => ({
    root: {
      flexGrow: 1,
      padding: 16,
      paddingTop: 'inherit'
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'left',
      color: theme.palette.text.secondary,
      height: '104%',
    },
    footer: {
      paddingTop: '35px'
    },
    mainHeader: {
      color: 'orange',
      marginBottom: '0px',
   }, 
   subHeader: {
      color: 'grey',
      marginBottom: 'auto',
   },
   per: {
        color: 'grey',

   }
  });


function PageTemplate  (props)  {
    const {classes} = props;

    return (
        <div>
            <Grid className={classes.root} container direction="column" spacing={8}>
                <Grid item xs>
                <h1 className={classes.mainHeader}> Page Template Exmaple </h1>
                <p className={classes.subHeader}> Basic structure of page in this portal. </p>
                </Grid>
                <Grid item xs>
                    <Typography className={classes.per} variant="body1" gutterBottom>
                    Lorem ipsum dolor sit amet, cibo mazim oporteat ut has, cu per alia illud voluptatum! In praesent iudicabit comprehensam usu, ut vix magna regione, nostro atomorum an usu! Debet zril scriptorem mel no, has novum albucius referrentur an, ut qui aperiri veritus! Mutat qualisque quo ut. Te sint nihil efficiantur vix, nam soleat timeam ne.
                    </Typography>
                    <Typography className={classes.per} variant="body1" gutterBottom>
                    Eum id inani doming regione, quo doming civibus vituperatoribus et, ea has mandamus qualisque? Brute dicit ullamcorper pro no, at elitr latine officiis mea. Mel habeo causae admodum id, viris cotidieque sea id. Vix et munere singulis concludaturque! In nam dico elitr laboramus. Noster mollis mea id.
                    </Typography>
                    <Typography className={classes.per} variant="body1" gutterBottom>
                    Lorem ipsum dolor sit amet, cibo mazim oporteat ut has, cu per alia illud voluptatum! In praesent iudicabit comprehensam usu, ut vix magna regione, nostro atomorum an usu! Debet zril scriptorem mel no, has novum albucius referrentur an, ut qui aperiri veritus! Mutat qualisque quo ut. Te sint nihil efficiantur vix, nam soleat timeam ne.
                    </Typography>
                    <Typography className={classes.per} variant="body1" gutterBottom>
                    Eum id inani doming regione, quo doming civibus vituperatoribus et, ea has mandamus qualisque? Brute dicit ullamcorper pro no, at elitr latine officiis mea. Mel habeo causae admodum id, viris cotidieque sea id. Vix et munere singulis concludaturque! In nam dico elitr laboramus. Noster mollis mea id.
                    </Typography>
                    
                </Grid>
            </Grid>
            <Grid className={classes.footer} item xs>
              <Footer/>
            </Grid>
        </div>
    );
}

export default withStyles(styles)(PageTemplate);

