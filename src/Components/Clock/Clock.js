import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';

   const styles = theme => ({    
    main: {
        float: 'right',
        // paddingLeft: '700px',
        color: 'inherit'
    },
    typo: {
        color: '#009fd9'
    },
    date: {
        color: 'inherit'
    },
   });

class Clock extends Component  {

    constructor (props) {
        super(props);
        this.state = {
            localTime: '',
            gmtTime: ''
        }
        this.startTime = this.startTime.bind(this);
        this.checkTime = this.checkTime.bind(this);
    }

    componentDidMount() {
        this.startTime();
    }

    startTime = () => {
        var today = new Date();
        let _localTime = '';
        let _gmtTime = '';
        var h = today.getHours();
        var m = today.getMinutes();
        m = this.checkTime(m);
        _localTime = h + ":" + m;
        if (h===0) {
            _gmtTime = (22) +":" + m;     
        } 
        else if (h===1) {
            _gmtTime = (23) +":" + m;     
        }
        else {
            _gmtTime = (h-2) +":" + m; 
        }
        var t = setTimeout(this.startTime, 1000);
        this.setState ({localTime: _localTime, gmtTime: _gmtTime});
      }
    
    checkTime = i => {
        if (i < 10) {i = "0" + i};  
        return i;
      }

    render () {
    const {classes} = this.props;
    return (
        <div>
            <Grid className={classes.main} container direction="row" spacing={0}>
                <Grid item xs>
                <Typography className={classes.typo} variant="button">{"Local Time: "+ this.state.localTime + " UTC: "+ this.state.gmtTime } </Typography>
                </Grid>
            </Grid>
        </div>
    );
    }
}

export default withStyles(styles)(Clock);