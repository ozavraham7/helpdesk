import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { Button, Grid } from '@material-ui/core';
import Clock from '../../Components/Clock/Clock';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({    
    root: {
        display: '-webkit-box' ,
        position: 'absolute',
        height: '80px',
        // margin: 'auto'
    },
    colorPrimary: {
        color: '#000000',
        background: '#eff0ef',
    },
    buttons: {
        margin: 'auto',
        right: 0,
        float: 'right',
        textAlign: 'center',
        paddingRight: '70px',
    },
    clock: {
        margin: 'auto',
        right: '0',
        float: 'right',
        textAlign: 'center'
    },
    mainHeader: {
        marginTop: 'auto',
        marginBottom: 'auto',
        paddingLeft: '5px',
    },
    mainHeaderColor: {
        color: 'orange'
    }
});

function TopNav (props)  {
    const { classes } = props;
        return (
            <div>
                <AppBar classes={{root: classes.root, colorPrimary: classes.colorPrimary, colorSecondary: classes.colorSecondary}} position="absolute">
                <Grid container alignItems="center" direction="row">
                    <Grid className={classes.mainHeader} item xs={2} sm={2} lg={3}>
                        <h1 className={classes.mainHeaderColor}> Helpdesk Portal </h1>
                    </Grid>
                    <Grid className={classes.buttons} item xs sm lg>
                        <Button color="inherit" href='' target="_blank" button="true" key='Link1'>Link1</Button>
                        <Button color="inherit" href='' target="_blank" button="true" key='Link2'>Link2</Button>
                        <Button color="inherit" href='' target="_blank" button="true" key='Link3'>Link3</Button>
                        <Button color="inherit" href='' target="_blank" button="true" key='Link4'>Link4</Button>
                        <Button color="inherit" href='' target="_blank" button="true" key='Link5'>Link5</Button>
                    </Grid>
                    <Grid className={classes.clock} item xs={2} sm={2} lg={2}>
                        <Clock/>
                    </Grid>
                </Grid>
                </AppBar>
            </div>
        );

}

export default withStyles(styles)(TopNav);

