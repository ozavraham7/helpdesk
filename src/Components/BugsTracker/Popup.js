import React, {Component} from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import axios from 'axios';
import { Dialog, Typography} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import {createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import { blue, orange} from '@material-ui/core/colors';

class Popup extends Component {

    constructor(props) {

        super(props);
        var _chosenHeader;
        if (props.isNew==="true") {
            _chosenHeader = "Add New Bug";
        }
        else _chosenHeader = "Edit Bug " + props.Bug;
       
        var teams_array = ["Data","Yoni", "SBM V4", "Sergey"];
        var products_array = ["OddService","SBM V3", "SBM V4", "LDS", "CDS","CRM","Hot-Odds"];
        // Defualt values
        var team_id = 0;
        var product_id = 0;

        // Need to extract previuos selections
        if (props.isNew === "false") {
            team_id = (teams_array.indexOf(props.Team));
            product_id =(products_array.indexOf(props.Product));
        }

        console.log(team_id, product_id);

        console.log("Inside Constructor with row" , props.row);
        this.state = {
            open: false,
            row: props.row,
            Id: props.ID,
            isNew: props.isNew,
            Product: props.Product,
            BugNumber: props.Bug,
            Title: props.Title,
            CustomerID: props.CustomerID,
            TicketId: props.TicketId,
            Team: props.Team,
            chosenHeader: _chosenHeader,
            buttonName: props.buttonName,
            chosenTeamID: team_id,
            chosenProductID: product_id 
        }

        this.ValidateData = this.ValidateData.bind(this);
        this.ticketChangeListener = this.ticketChangeListener.bind(this);
        this.bugChangeListener = this.bugChangeListener.bind(this);
        this.ticketChangeListener = this.ticketChangeListener.bind(this);
        this.customerIDChangeListener = this.customerIDChangeListener.bind(this);
        this.DeleteBug = this.DeleteBug.bind(this);
    }

    componentWillReceiveProps(newProps) {
        console.log("Inside componentWillReceiveProps" , newProps);
        var teams_array = ["Data","Yoni", "SBM V4", "Sergey"];
        var products_array = ["OddService","SBM V3", "SBM V4", "LDS", "CDS","CRM","Hot-Odds"];
        var team_id = (teams_array.indexOf(newProps.Team));
        var product_id =(products_array.indexOf(newProps.Product));
        var _chosenHeader = "Edit Bug " + newProps.Bug;
        this.setState ( {
            row: newProps.row,
            Id: newProps.ID,
            isNew: newProps.isNew,
            Product: newProps.Product,
            BugNumber: newProps.Bug,
            Title: newProps.Title,
            CustomerID: newProps.CustomerID,
            TicketId: newProps.TicketId,
            Team: newProps.Team,
            chosenHeader: _chosenHeader,
            buttonName: newProps.buttonName,
            chosenTeamID: team_id,
            chosenProductID: product_id 
        })
        }
    
    titleChangeListener = (event) => {
        const _title = event.target.value;
        this.setState({Title: _title});
        console.log(this.state);
    }

    bugChangeListener = (event) => {
        const _bug = event.target.value;
        this.setState({BugNumber: _bug});
        console.log(this.state);
    }

    ticketChangeListener = (event) => {
        const _ticket = event.target.value;
        this.setState({TicketId: _ticket});
        console.log(this.state);
    }

    customerIDChangeListener= (event) => {
        const _customer = event.target.value;
        this.setState({CustomerID: _customer});
        console.log(this.state);
    }

    teamChangeListener= (event) => {
        const _team = event.target.value;
        this.setState({Team: _team});
    }

    productChangeListener= (event) => {
        const _product = event.target.value;
        this.setState({Product: _product});
    }

    handleClickOpen = () => {
        console.log("Current Popup state: "  , this.state);
        this.setState({ open: true });
      };

      handleClose = () => {
        this.setState({ open: false });
      };

    render(){

        var teams_array = [
            {value: "Data", key: "Data"},
            {value: "Yoni", key: "Yoni"}, 
            {value: "SBM V4", key: "SBM V4"},
            {value: "Sergey", key: "Sergey"}];
        var products_array = [
            {value: "OddService", key: "OddService"},
            {value: "SBM V3", key: "SBM V3"}, 
            {value: "SBM V4", key: "SBM V4"}, 
            {value: "LDS", key: "LDS"}, 
            {value: "CDS", key: "CDS"},
            {value: "CRM", key: "CRM"},
            {value: "Hot-Odds", key: "Hot-Odds"}];

            const myTheme = createMuiTheme({
                palette: {
                    primary: orange, 
                    secondary: blue,
                }
            });

        return (
            <div>
            <MuiThemeProvider theme={myTheme}>
                <Button variant="contained" color="secondary" onClick={this.handleClickOpen} style = {{marginLeft: 20}}> {this.state.buttonName} </Button>
                <Dialog
                open = {this.state.open}
                header={this.state.chosenHeader}
                onClose={this.handleClose} 
                maxWidth="md"
                fullWidth
                >
                <Grid container className="dialogContainer" spacing={16}  direction="column" style={{margin: 8, paddingLeft: 85}}> 
                    {/* ROW 1 HEADER */}
                    <Grid item xs={12}>
                        <Typography variant="h6" color="inherit" style={{margin: 8}}>
                            {this.state.chosenHeader}
                        </Typography>
                    </Grid>
                    {/* ROW 2 TITLE */}
                    <Grid item xs={11}>
                        <TextField onChange={this.titleChangeListener} defaultValue={this.state.Title} type="text" label="Title" fullWidth required  />
                    </Grid>
                    {/* ROW 3 SELECTES */}
                    <Grid id="selectContainer" container justify="center" alignContent="center" style={{margin: 8}}>
                        <Grid item xs>
                            <TextField
                                id="outlined-select"
                                select
                                label="Product"
                                value={this.state.Product}
                                onChange={this.productChangeListener}
                                SelectProps={{
                                }}
                                helperText="Please select the relevent product"
                                margin="normal"
                                variant="outlined"
                                required
                                style={{ margin: 8}}
                            >
                            {products_array.map(option => (
                                <MenuItem key={option.key} value={option.value}>{option.value}</MenuItem>
                            ))}
                            </TextField>
                        </Grid>
                        <Grid item xs >
                            <TextField
                                id="outlined-select"
                                select
                                label="Team"
                                value={this.state.Team}
                                onChange={this.teamChangeListener}
                                SelectProps={{
                                }}
                                helperText="Please select the relevent Team"
                                margin="normal"
                                variant="outlined"
                                required
                                style={{ margin: 8}}
                            >
                            {teams_array.map(option => (
                                <MenuItem key={option.key} value={option.value}>{option.value}</MenuItem>
                            ))}
                            </TextField>
                        </Grid>
                    </Grid>
                    {/* ROW 4 BUG# TEAM AND PRODUCT */}
                    <Grid container spacing={16} justify="center" alignItems="center" style={{margin: 8}}>
                        <Grid item xs>
                            <TextField onChange={this.bugChangeListener} defaultValue={this.state.BugNumber} label="Bug #:" required></TextField>
                        </Grid> 
                        <Grid item xs>
                            <TextField onChange={this.ticketChangeListener} defaultValue={this.state.TicketId} label="Ticket:"></TextField>
                        </Grid>
                        <Grid item xs>
                            <TextField onChange={this.customerIDChangeListener} defaultValue={this.state.CustomerID} label="Customer Id:"></TextField>
                        </Grid>
                    </Grid>
                       {/* ROW 5 ACTIONS */}
                       <Grid container justify="center" alignContent="space-around">
                            <DialogActions>
                            <Button variant="contained" color="primary" onClick={this.ValidateData}> {this.state.chosenHeader} </Button>
                            <Button variant="contained" color="secondary" onClick={this.DeleteBug} disabled={this.state.isNew==="true" ? true : false}> Remove </Button>
                            </DialogActions>
                        </Grid>
                </Grid>
                </Dialog>
            </MuiThemeProvider>
            </div>
        );
    }

    ValidateData() {


        /*
            Add Title validation (contains  ')
            CustomerID validation (only numbers!)
        */

        console.log("Inside ValidateData()");
        var product = document.getElementsByName("products");
        product.forEach(element => {    
            if (element.checked) this.setState({Product: element.value});
        });
    
        var teams = document.getElementsByName("team");
        teams.forEach(element => {    
            if (element.checked) this.setState({Team: element.value});
        });

        var _ticket = this.state.TicketId;
        // Remove unnecesery #
        if (_ticket.startsWith("#")) {
            _ticket = _ticket.substring(1);
            this.setState({TicketId: _ticket})
        };
    
        if (this.state.Title==="" || this.state.Bug==="" || this.state.Product==="" || this.state.Team ==="") {  
            /* Need to add feedback to user using Snackbars */ 
            console.log("inside if");
        }
        else {
            console.log("current popup status: " ,this.state.buttonName);
            if (this.state.isNew==="true") this.AddBug();
            else this.EditBug();
        } 
    }
    
    AddBug() {
        console.log("Inside AddBug()");
        var newBug = this.state;
        // console.log(newBug);
        axios ({
            method:'POST',
            url: 'localhost:3000/addNewBugtest',
            json:true,
            headers: {
                'Content-Type': 'application/json'
            }, // Send here isActive 1 as well
            data: {
                Product: newBug.Product,
                BugNumber: newBug.BugNumber,
                Title: newBug.Title,
                Customer: newBug.CustomerID,
                Ticket: newBug.TicketId,
                Team: newBug.Team,
            }
        }).then(Respone => {
            if (Respone.data.Success===true)
            {
                // window.Materialize.toast("Success!", 4000);
                // window.Materialize.toast("Started tracking bug #" + newBug.BugNumber, 4000);
                window.alert("Started tracking bug #" + newBug.BugNumber);
                window.location.reload();
            }
            else {
                // window.Materialize.toast("Somthing went wrong, check yourself", 4000);
                window.alert("Somthing went wrong\nCheck ITServer1");
            }
        });
    }

    EditBug() {
        console.log("Inside EditBug()");
        var newBug = this.state;
        console.log("Inside EditBug()");
        // console.log("Inside EditBug() Sending " , newBug);
        axios ({
            method:'POST',
            url: 'localhost:3000/updateBugRecord',
            json:true,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                Id: newBug.Id,
                Product: newBug.Product,
                BugNumber: newBug.BugNumber,
                Title: newBug.Title,
                Customer: newBug.CustomerID,
                Ticket: newBug.TicketId,
                Team: newBug.Team,
            }
        }).then( Respone => {console.log(Respone);
                if (Respone.data.Success===true) {
                    // window.Materialize.toast("Bug Details updated succefully", 4000);
                    window.alert("Bug Details updated succefully");
                    window.location.reload();
                }
                else {
                    // window.Materialize.toast(", check yourself", 4000);
                    window.alert("Somthing went wrong\nCheck ITServer1");
                }}
        );
    }

    // Change to stopTracking with isActive 0
    DeleteBug(){
        console.log("Inside DeleteBug() CurrentState: " , this.state);
        var newBug = this.state;
        console.log("Inside DeleteBug()");
        axios ({
            method:'POST',
            url: 'http://172.20.2.31:4000/deleteBugRecord',
            json:true,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                Id: newBug.Id,
                Product: newBug.Product,
                BugNumber: newBug.BugNumber,
                Title: newBug.Title,
                Customer: newBug.CustomerID,
                Ticket: newBug.TicketId,
                Team: newBug.Team,
            }
        }).then( Respone => {console.log(Respone);
                if (Respone.data.Success===true) {
                    // window.Materialize.toast("Bug " + newBug.Id + "deleted succefully", 4000)
                    window.alert("Bug " + newBug.Id + "deleted succefully");
                    window.location.reload();
                }
                else {
                    // window.Materialize.toast("Somthing went wrong, check yourself", 4000);
                    // window.Materialize.toast("Or contact Oz ;)" , 4000);
                    window.alert("Somthing went wrong\nCheck ITServer1");
                }}
        );
    }

}


export default Popup;
