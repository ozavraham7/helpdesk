
import React from 'react';
import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Recents from '../Recent/Recents'
import Footer from '../../Structure/Footer/Footer';


const styles = theme => ({
    root: {
      flexGrow: 1,
      padding: 16,
      paddingTop: 'inherit'

    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'left',
      color: theme.palette.text.secondary,
      height: '104%',
    },
    mainHeader: {
      color: 'orange',
      marginBottom: '0px',
   }, 
   subHeader: {
      color: 'grey',
      marginBottom: 'auto',
   },
  });

const RecentUpdates = (props) => { 
    const {classes} = props;
    return (
        <div>
            <Grid className={classes.root} container direction="column" spacing={8}>
              <Grid item xs>
                  <h1 className={classes.mainHeader}> Recent Updates </h1>
                  <p className={classes.subHeader}>  Present all of the updates that were inserted by previous helpdesk operators. </p>
              </Grid>
              <Grid item xs>
                  <Recents all={true}/>
              </Grid>
             </Grid>
             <Grid item xs>
                  <Footer/>
              </Grid>
        </div>
    );
  }

export default withStyles(styles)(RecentUpdates);
