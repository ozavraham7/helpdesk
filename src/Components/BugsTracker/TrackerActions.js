import React, {Component} from 'react';
import { createMuiTheme } from '@material-ui/core';
import { blue, orange, grey, white} from '@material-ui/core/colors';
import './TrackerActions.css';

/* 
  Bugs tracking tool - TrackerActions.js
  Last update: 12/01/19
  Author: Oz Avraham 
*/ 

const TrackerActions = () => {

    
        const myTheme = createMuiTheme({
            palette: {
                primary: orange, 
                secondary: blue,
                textSecondary: grey
            },
            overrides: {
                Typography :{
                    textPrimary: white
                }
            }
        });

        return (
            <div className="ActionsContainer">
                <h1> Bugs Tracking </h1>
                <p>
                    Track all recent open bugs 
                </p>
            </div>
        )
    }



export default TrackerActions;
