import React, {Component} from 'react';
import ReactTable from 'react-table';
import Link from '@material-ui/core/Link';
import {bugsExamples} from '../../Assest/ExamplesData';
import 'react-table/react-table.css'

class BugsRecords extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    /* Fetching records */
    componentDidMount() {
        this.setState({data: bugsExamples});
    }

    render () {

        var blueStates = ["Fixed","Testing In Prod","Approved","In Progress","In Testing"];
        var greyStates = ["Removed", "Deferred", "Rejected"];
        const _columns = [
            {Header: "ID", accessor: "Id", minWidth:30},
            {Header: "CreationDate", accessor: "CreationDate", minWidth:70 ,Cell: row => {
                return (<span>{row.value.substring(0,16).replace("T"," ")}</span>);
             }},
            {Header: "Bug", accessor: "BugNumber",minWidth:40, Cell: row => {
                var link = "http://tfs:8080/tfs/Development/Dev/_workitems?id=" + row.original.BugNumber + "&_a=edit";
                return (<Link href={link} target="_blank" rel="noreferrer"> {row.original.BugNumber} </Link>);
            }},
            {Header: "Title", accessor: "Title", minWidth: 200,style:{textAlign: 'left'}, Cell: row => {
                return (<span style={{alignItems: 'left'}}>{row.value}</span>)
            }},
            {Header: "CustomerID", accessor: "Customer",minWidth:50},
            {Header: "Ticket", accessor: "Ticket",minWidth:50, Cell: row => {
                if (row.value==="Internal") return (<span>{row.value}</span>);
                var link ="https://lsports.ladesk.com/agent/index.php?rnd=8051#Conversation;id=" + row.original.Ticket;
                return (<Link href={link} target="_blank" rel="noreferrer"> {row.original.Ticket} </Link>);
            }},
            {Header: "Team", accessor: "Team",minWidth:50, Cell: row=> {
                var str = row.value;
                if (str.includes("Products")) return (<span>{row.value.substring(13)}</span>);
                else return (<span>{row.value.substring(4)}</span>)
            }},
            {Header: "State", accessor: "State",minWidth:50,style:{textAlign: 'left'}, Cell: row => {
                var _color = "";
                if (row.value==="") return (""); // If no state (old bugs)
                if (row.value==="New") _color = "#f44336";
                else if (blueStates.indexOf(row.value) !== -1) _color = "#2196f3";
                else if (row.value==="Closed") _color = "#8bc34a";
                else if (greyStates.indexOf(row.value) !== -1) _color="#607d8b";
                else _color="#ffeb3b";
                return (<span><span style={{color: _color}}>● </span>{row.value}</span>);
            }},

    ];

        return (
                <ReactTable
                columns={_columns}
                className="-highlight"
                defaultSorted={[{id:"ID", desc: true}]}
                data = {this.state.data}
                defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]).toLocaleLowerCase().includes(filter.value.toLocaleLowerCase())}
                resolveData={_data => _data.map(row => row)}
                defaultPageSize={10}
                filterable
                style={{width: "100%"}}
                loadingText= 'Loading...'
                noDataText= 'Unable to fetch records, check Local server'
                />
        )
    }
}

export default BugsRecords;


