import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import {NavLink} from "react-router-dom";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    
    drawer: {
        width: '210px',
        flexShrink: 0,
        zIndex: 1,
    },
    drawerPaper: {
        position:'absolute',
        flexShrink: 0,
        zIndex: 1,
        width: '210px',
        padding: '70px 0px 0px 0px',

    }
});

class ListItemLink extends React.Component {
    renderLink = itemProps => <NavLink to={this.props.to} {...itemProps} />;
  
    render() {
      const { primary } = this.props;
      return (
        <li>
          <ListItem button component={this.renderLink}>
            <ListItemText primary={primary} />
          </ListItem>
        </li>
      );
    }
  }
  
const SideNav = (props) => {
    const { classes } = props;
    return (
        <div>      
                <Drawer className ={classes.drawer} variant="permanent" anchor="left" classes={{paper: classes.drawerPaper}}>
                    <List component="nav">
                      <ListItemLink to="/" primary='Home'/> 
                      <Divider/>
                      <ListItemLink to="/bugstracker" primary='BugsTracking'/> 
                      <Divider/>
                      <ListItemLink to="/recentupdates" primary='Recent Updates'/> 
                      <Divider/>
                      <ListItemLink to="/frequentaq" primary='Frequent Asked Q'/> 
                      <Divider/>
                      <ListItemLink to="/pagetemplate" primary='Page Template'/> 
                      <Divider/>
                    </List>
                </Drawer>
        </div>

    );
}

SideNav.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(SideNav);
