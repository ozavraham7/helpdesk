import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Footer from '../../Structure/Footer/Footer';
import BugsRecords from './BugsRecords';

const styles = theme => ({
    root: {
      flexGrow: 1,
      padding: 16,
      paddingTop: 'inherit',
      paddingBottom: '0px',
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'left',
      color: theme.palette.text.secondary,
      height: '104%',
    },
    mainHeader: {
      color: 'orange',
      marginBottom: '0px',
    }, 
    subHeader: {
      color: 'grey',
      marginBottom: 'auto',
    },
    table: {
      paddingTop: '10px',
    }
  });

class BugsTracker extends Component {

    render () {
        const {classes} = this.props;

        return (
            <div>
              <Grid container spacing={0} className={classes.root} direction="column">
                  <Grid item xs>
                  <h1 className={classes.mainHeader}> Bugs Tracking </h1>
                  <p className={classes.subHeader}> This tool is integrated with the company local version control, catching any new items created and tracking them. </p>
                  </Grid>
                  <Grid className={classes.table} item xs> <BugsRecords/> </Grid>
              </Grid>
              <Grid item xs> <Footer/> </Grid>
            </div>
        )
    }
}

export default withStyles(styles)(BugsTracker);