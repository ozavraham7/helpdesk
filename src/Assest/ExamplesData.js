export const bugsExamples = [
    {
        Id: '1',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #1',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'New'
    },
    {
        Id: '2',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1357',
        Title: 'Example title bug #2',
        Customer: '1653',
        Ticket: 'fdsg54s',
        Team: 'Team2',
        State: 'Closed'
    },
    {
        Id: '3',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1987',
        Title: 'Example title bug #3',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'New',
        State: 'Testing in prod'
    },
    {
        Id: '4',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #4',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'Fixed'
    },
    {
        Id: '5',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #5',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'Removed'
    },
    {
        Id: '6',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #6',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'Fixed'
    },
    {
        Id: '7',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #7',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'New'
    },
    {
        Id: '8',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #8',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'Rejected'
    },
    {
        Id: '9',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #9',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'New'
    },
    {
        Id: '10',
        CreationDate: '2019-03-25 18:15',
        BugNumber: '1234',
        Title: 'Example title bug #10',
        Customer: '1234',
        Ticket: '123fd5',
        Team: 'Team1',
        State: 'New'
    },
]
export const fqaExamples = [
    {
        id: '1',
        time: '20/3/2019 11:52',
        question: 'Example # 1' ,
        answer: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
    },
    {
        id: '2',
        time: '20/3/2019 11:52',
        question: 'Example # 2' ,
        answer: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
    },
    {
        id: '3',
        time: '20/3/2019 11:52',
        question: 'Example # 3' ,
        answer: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
    },
    {
        id: '4',
        time: '20/3/2019 11:52',
        question: 'Example # 4' ,
        answer: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
    },
    {
        id: '5',
        time: '20/3/2019 11:52',
        question: 'Example # 5' ,
        answer: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
    },
    {
        id: '6',
        time: '20/3/2019 11:52',
        question: 'Example # 6' ,
        answer: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
    },

    
]    
export const updatesExamples = [
    {
        id: '1',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '2',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '3',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '4',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '5',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '6',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '7',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '8',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '9',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '10',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },
    {
        id: '11',
        text: 'Lorem ipsum dolor sit amet, in ius impedit volumus, duo an volumus singulis volutpat. Ius ad etiam erant conclusionemque, eam et modus docendi molestie. Illum reprimique an pro, ius et verear dolores liberavisse, ne tota accumsan intellegat per! Reque ludus oportere vim cu. Minim latine detraxit vix ex, ludus elaboraret ut nec!',
        time: '20/3/2019 11:52',
    },

]