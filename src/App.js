import React, { Component } from 'react';
import TopNav from '../src/Structure/TopNav/TopNav';
import Main from '../src/Structure/Main/Main';

class App extends Component {
  
  render() {
    return (
          <div className="App">
            <TopNav/>
            <Main/>
          </div>
    );
  }
}

export default App;
