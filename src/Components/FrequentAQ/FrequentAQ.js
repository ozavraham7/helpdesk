import React, {Component} from 'react';
import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Footer from '../../Structure/Footer/Footer';
import  {TextField, Button} from '@material-ui/core';
import {Dialog, DialogContent,DialogActions} from '@material-ui/core';
import DialogTitle from '@material-ui/core/DialogTitle';
import Snackbar from '@material-ui/core/Snackbar';
import AQItems from './AQItems';
import green from '@material-ui/core/colors/green';
import Fade from '@material-ui/core/Fade';
import {fqaExamples} from '../../Assest/ExamplesData';


const styles = theme => ({
    root: {
      flexGrow: 1,
      padding: 16,
      paddingTop: 'inherit'

    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'left',
      color: theme.palette.text.secondary,
      height: '104%',
    },
    inputContainer: {
      width: '70%',
    },
    subBtn: {
      color: '#009fd9',
      primary: '#009fd9',
      secondary: '#009fd9',
    },
    items: {
      paddingTop: '10px'
    },
    successSnack: {
      backgroundColor: green[600],
    },
    faildSnack: {
      backgroundColor: theme.palette.error.dark,
    },
    mainHeader: {
      color: 'orange',
      marginBottom: '0px',
   }, 
   subHeader: {
      color: 'grey',
      marginBottom: 'auto',
   },
  });

  class FrequentAQ extends Component {

    constructor(props){
      super(props);
      this.state = {
        question: '',
        answer: '',
        data: [],
        open: false,
        isEdit: false,
        editID: '',
        editIndex: '',
        successSnack: false,
        failSnack: false,
        message: '',
      }
      this.textChangeListener = this.textChangeListener.bind(this);
      this.clearFields = this.clearFields.bind(this);
      this.submitListener = this.submitListener.bind(this);
      this.editListener = this.editListener.bind(this);
      this.handleOpen = this.handleOpen.bind(this);
      this.deleteLisenter = this.deleteLisenter.bind(this);
      this.editSubmitListener = this.editSubmitListener.bind(this);
      this.handleClose = this.handleClose.bind(this);
      this.handleSnackClose = this.handleSnackClose.bind(this);
    }

    /* Fetching Records  */
    componentDidMount (){
      var _fqasList = fqaExamples;
      this.setState({data: _fqasList});
    }

    /* Listening to Text changes */
    textChangeListener = (event) => {
      this.setState({[event.target.name]: event.target.value})
    }

    /* Clear fields in Input Modal */
    clearFields = () => {
      this.setState({question: '', answer: ''})
    }

    /* Close Input Modal */
    handleClose = () => {
      this.setState({open: false, question:'', answer: ''});
    }

    /* Open Input Modal */
    handleOpen = () => {
      this.setState({open: true, question:'', answer: '', isEdit: false});
    }

    /* Submmit new Q&A */
    submitListener = (event) => {
      let item = {
        question: this.state.question,
        answer: this.state.answer,
        time: this.generateDate()
      }
      if (item.question !== "" | item.answer !== "") {
        let qaItems = [...this.state.data];
        qaItems.unshift(item);
        this.setState({data: qaItems, question: '', answer: '', open: false, successSnack: true, message: 'Added Successfully'});
      }
      else {
        this.setState({failSnack: true, message:'Missing input!'})
      }
    }

  /* Submit Edit request */
    editSubmitListener = (event) => {
      let item = {
        question: this.state.question,
        answer: this.state.answer,
        date: this.generateDate()
      }
      if (item.question !== "" | item.answer !== "") {
        var qaItems = [...this.state.data];
        var index = this.state.editIndex;
        qaItems[index].answer = item.answer; 
        qaItems[index].question = item.question;
        this.setState({question: '', answer: '', open: false, isEdit: false, editID: '', editIndex: '',successSnack: true, message:'Edited Succefully'})
      }
      else {
        this.setState({failSnack: true, message:'Missing input!'})
      }
    }

    /* Open Edit Modal */
    editListener = (index) => {
      let qaItems = [...this.state.data];
      var id = qaItems[index].id;
      this.setState({question: qaItems[index].question, answer: qaItems[index].answer, open: true, isEdit: true, editID: id, editIndex: index});
    }

    /* Removing existing data */
    deleteLisenter = (cardIndex) => {
      var qaItems = [...this.state.data];
      qaItems.splice(cardIndex,1);
      this.setState({data:qaItems, successSnack: true, message:'Deleted Succefully'});
    }

    /* Gerate current data and time */
    generateDate() {
      var date = new Date();
      var hours = date.getHours();
      var minutes = date.getMinutes();
      if (hours<10) {
        hours = '0' + hours.toString();
      }
      if (minutes<10) {
        minutes = '0' + minutes.toString();
      }
      var newTime =  (date.getDate() + '/' + Number(date.getMonth()+1)+ '/' + date.getFullYear() + ' ' + hours + ':' + minutes);
      return newTime;
    }

    handleSnackClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
      this.setState({ failSnack: false, successSnack: false, message:'' });
    };

      render(){
        const {classes} = this.props;
        return (
            <div>
                <Grid className={classes.root} container direction="column" spacing={8}>
                <Grid item xs>
                    <h1 className={classes.mainHeader}> Frequent Asked Questions </h1>
                    <p className={classes.subHeader}> Find here answers  questions that were asked before, or by our team or by customer. <br/>
                    This should help us to explain to the customer how our products are WORKING and BEHAVING in certain cases. <br/>
                    Help each other to maintain this document each time you encountering difficult/uknown questions from customer. <br/>
                    Add some questions you encounterd with  and the relevant answer:
                    </p>
                </Grid>
                <Grid item xs>
                <Grid className={classes.inputContainer} container direction="column" spacing={0}>
                <Grid item><Button className={classes.subBtn} size="medium" variant="outlined" onClick={this.handleOpen} > Add new Q&A </Button></Grid>
                </Grid>
                <Grid item className={classes.items}>
                <AQItems data={this.state.data} editSubmitListener={this.editListener} deleteLisenter={this.deleteLisenter}/>
                </Grid>
                <Dialog
                  className={classes.root}
                  open={this.state.open}
                  onClose={this.handleClose}
                  aria-labelledby="draggable-dialog-title"
                  fullWidth
                  >
                  <DialogTitle id="form-dialog-title">{this.state.isEdit ? "Edit Q&A" : "Add new Q&A"}</DialogTitle>
                  <DialogContent>
                  <TextField
                      id="outlined-textarea"
                      defaultValue={this.state.question}
                      multiline
                      label="Question"
                      name="question"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      className={classes.textfield}
                      onChange={this.textChangeListener}
                  />
                  <TextField
                      id="outlined-textarea"
                      defaultValue={this.state.answer}
                      multiline
                      label="Answer"
                      name="answer"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      className={classes.textfield}
                      onChange={this.textChangeListener}
                  />
                  </DialogContent>
                  <DialogActions>
                      <Button className={classes.subBtn} onClick={this.handleClose} > Cancel </Button>
                      <Button className={classes.subBtn} onClick={this.state.isEdit ? this.editSubmitListener : this.submitListener} > {this.state.isEdit ? "Confirm" : "Add"} </Button>
  n                </DialogActions>
                  </Dialog>
                </Grid>
                <Grid item xs>
                    <Footer/>
                  </Grid>
                </Grid>
                <Snackbar
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={this.state.successSnack}
                  autoHideDuration={3000}
                  onClose={this.handleSnackClose}
                  ContentProps={{
                    'aria-describedby': 'message-id',
                    className: classes.successSnack
                  }}
                  TransitionComponent={Fade}
                  message={this.state.message}
                />
                <Snackbar
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={this.state.failSnack}
                  autoHideDuration={3000}
                  onClose={this.handleSnackClose}
                  ContentProps={{
                    'aria-describedby': 'message-id',
                    className: classes.faildSnack
                  }}
                  TransitionComponent={Fade}
                  message={this.state.message}
                />
            </div>
        );
    }
}

export default withStyles(styles)(FrequentAQ);
