import React, { Component, Fragment } from 'react';
import { BrowserRouter, Switch, Route} from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import BugsTracker from '../../Components/BugsTracker/BugsTracker'
import HomePage from '../../Components/HomePage/HomePage';
import HorseRacingSources from '../../Components/HorseRacingSources/HorseRacingSources';
import InfraComponents from '../../Components/InfraComponents/InfraComponents';
import NocShifts from '../../Components/NocShifts/NocShifts';
import SideNav from '../SideNav/SideNav';
import DBActions from '../../Components/DBActions/DBActions';
import CustomerOrders from '../../Components/CustomerOrders/CustomerOrders';
import FrequentQA from '../../Components/FrequentQA/FrequentQA';
import RecentUpdates from '../../Components/RecentUpdates/RecentUpdates';
import BugsGuidelines from '../../Components/BugsGuidelines/BugsGuidelines';
import FixtureLifeCycle from '../../Components/FixtureLifeCycle/FixtureLifeCycle';

class Main extends Component  {

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Fragment>
          <div className="MainDiv">   
            <SideNav/> 
            <Route exact path="/" component={HomePage}/>
            <Route path="/bugstracker" component={BugsTracker}/>
            <Route path="/horseRacing" component={HorseRacingSources}/>
            <Route path="/infracomponents" component={InfraComponents}/>
            <Route path="/recentUpdates" component={RecentUpdates}/>
            <Route path="/FrequentQA" component={FrequentQA}/>
            <Route path="/orders" component={CustomerOrders}/>
            <Route path="/shifts" component={NocShifts}/>
            <Route path="/dbactions" component={DBActions}/>
            <Route path="/bugsguide" component={BugsGuidelines}/>
            <Route path="/fixtureLifeCycle" component={FixtureLifeCycle}/>
          </div>
          </Fragment>
        </Switch>
      </BrowserRouter> );
  }
}
     
const styles = theme => ({
  root: {
  },
  content: {
    
  },
  toolbar: theme.mixins.toolbar,
});

Main.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Main);