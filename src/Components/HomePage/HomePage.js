import React from 'react';
import { Grid, Divider } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Recents from '../Recent/Recents';
import Link from '@material-ui/core/Link';
import Footer from '../../Structure/Footer/Footer';
import WifiIcon from '@material-ui/icons/Wifi';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import UpdateIcon from '@material-ui/icons/Update';

const styles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'left',
      color: theme.palette.text.secondary,
      height: '100%',
      paddingTop: '2px',
      boxShadow: 'none'

    },
    recentsPaper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'left',
        color: theme.palette.text.secondary,
        height: '100%',
        paddingTop: '2px',
        maxHeight: '900px',
        position: 'absolute',
        overflowY: 'scroll',
        boxShadow: 'none',
      },
    main: {
        display: 'flex',
        paddingTop: 'inherit',
    },
     footer: {
         paddingTop: '50px'
     },
     link: {
         color: '#009fd9'
     },
     mainHeader: {
        color: 'orange',
        marginBottom: '0px',
     }, 
     subHeader: {
        color: 'grey',
        marginBottom: 'auto',
     },
     subPer: {
        color: 'grey'
     },
     updates: {
     }
  });


const HomePage = (props) => {
    const { classes } = props;
    return (
        <div className="Home">
            <Grid className={classes.main} container spacing={0}>
                <Grid item xs={8} sm={8} lg={8}>
                    <Paper className={classes.paper}>
                    <h1 className={classes.mainHeader}> Daily Shift Routine </h1>
                    <h2 className={classes.subHeader}>Catching up</h2>
                    <p className={classes.subPer}>
                    • Check all E-Mails since last shift<br/>
                    • Read all Slack/Skype Correspondences<br/>
                    • Preform and Overlap with previoues helpdesk operator<br/>
                    </p>
                    <h2 className={classes.subHeader}>Setting up the shift</h2>
                    <p className={classes.subPer}>
                    • Connect to all relevent internal tools<br/>
                    • Update team leader regarding last shift exceptions<br/>
                    </p>
                    <h2 className={classes.subHeader}>During the shift</h2>
                    <p className={classes.subPer}>
                    • Stay alert to Skype conversations<br/>
                    • Stay alert for incoming tickets<br/>
                    • Stay alert for incoming servers alerts and warnings<br/>
                    </p>
                    <Divider/>
                    <Grid item xs>
                    <h1 className={classes.mainHeader}>Administrative and Test Accounts</h1>
                    <p className={classes.subPer}>
                        <WifiIcon/> Wifi Password: Password <br/>
                        <FastfoodIcon/> <Link className={classes.link} href="" target="_blank" rel="noreferrer">Order</Link> something to eat!<br/>
                    </p>
                </Grid>
                <h2 className={classes.subHeader}> Product Test Account </h2>
                <Grid container spacing={0} direction="row">
                <Grid item xs>
                    <h3 className={classes.subHeader}>Package1</h3>
                    <p className={classes.subPer}>
                        • <b>Email:</b> email1 <br/>
                        • <b>Password:</b> password1 <br/>
                    </p>
                </Grid>
                <Grid item xs>
                    <h3 className={classes.subHeader}>Package2</h3>
                    <p className={classes.subPer}>
                        • <b>Username:</b> email2 <br/>
                        • <b>Password:</b> password2 <br/>
                    </p>
                </Grid>
                </Grid>
                </Paper>
                </Grid>
            <Grid className={classes.updates} item xs={4} sm={4} lg={4}>
                <Paper className={classes.recentsPaper}>
                <Grid item> <h1 className={classes.mainHeader}> Last 10 Updates <UpdateIcon fontSize="large"/></h1></Grid>
                <Recents all={false}/>
                </Paper>
            </Grid>
            <Grid className={classes.footer} item xs>
                <Footer/>
            </Grid>
            </Grid>
        </div>
    )
}

export default withStyles(styles)(HomePage);