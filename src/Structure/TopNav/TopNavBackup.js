import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { Button } from '@material-ui/core';
import logo from '../../Images/portalLogo.png';
import Clock from '../../Components/Clock/Clock';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({    
    root: {
        display: '-webkit-box' ,
        position: 'absolute'
    },
    colorPrimary: {
        color: '#000000',
        background: '#eff0ef',
    },
});

function TopNav (props)  {
    const { classes } = props;
        return (
            <div className="topNavContainer">
                <AppBar className="topNav" classes={{root: classes.root, colorPrimary: classes.colorPrimary, colorSecondary: classes.colorSecondary}} position="absolute">
                <img src={logo} alt="logo" width="211" height="68px" />
                    <ul className="buttons">
                        <Button color="inherit" href='http://lds.lsports.eu/' target="_blank" button="true" key='LDS'>LDS</Button>
                        <Button color="inherit" href='http://crm.lsports.eu' target="_blank" button="true" key='CRM'>CRM</Button>
                        <Button color="inherit" href='http://client.lsports.eu/Welcome/Home' target="_blank" button="true" key='CDS'>CDS</Button>
                        <Button color="inherit" href='http://monitoring.lsports.eu' target="_blank" button="true" key='newLMS'>newLMS</Button>
                        <Button color="inherit" href='http://lms.lsports.eu/' target="_blank" button="true" key='oldLMS'>oldLMS</Button>
                        <Button color="inherit" href='https://site24x7.com/app/client#/home/monitors' target="_blank" button="true" key='Site24'>Site24</Button>
                        <Button color="inherit" href='https://calendar.google.com/calendar/r' target="_blank" button="true" key='LsportsCalender'>Calender</Button>
                        <Button color ="inherit" href='http://shiftmanager:7777/' target="_blank" button="true" key='ShiftManager'> ShiftManager </Button>
                        <Button color = "inherit" href='http://itserver1:5000/#/home?_k=6tjjsz' target="_blank" button="true" key='NocWiki' > Wiki </Button>
                        <Button disabled={true}><Clock/></Button>
                    </ul>
                </AppBar>
            </div>
        );

}

export default withStyles(styles)(TopNav);

