import React from 'react';
import {Typography,IconButton} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { Grid} from '@material-ui/core';

const styles = theme => ({
    card: {
      marginBottom: '15px',
      width: '90%',
    },
    media: {
      objectFit: 'cover',
    },
    colorPrimary: {
        color: '#000000',
        background: '#eff0ef',
    },
    margin: {
        float: 'right',
    },
    actions: {
        paddingLeft: '15px',
        background: '#eff0ef',
        paddingTop: '0px',
        paddingBottom: '0px',
    },
    actionsIcons: {
        marginLeft: 'auto'
    }
  });

const Updates = (props) => {
    const {classes} = props;
        return props.updates.map((_update,index) => {
            return(
                <div className="update" key={_update.id}>
                    <Card className={classes.card}>
                        <CardActionArea>
                            <CardContent>
                            <Typography component="p"> {_update.text} </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions className={classes.actions}>
                        <Grid item>
                            <Typography component="p"> Submitted on : {_update.time} </Typography>
                        </Grid>
                        <Grid item className={classes.actionsIcons}>
                            <IconButton aria-label="Delete" className={classes.margin} onClick={() => props.archiveClick(index)}>
                                <DeleteIcon fontSize="small"/>
                            </IconButton>
                            <IconButton aria-label="Edit" className={classes.margin} onClick={() => props.editLisenter(index)} >
                                <EditIcon fontSize="small"/>
                            </IconButton>
                        </Grid>
                        </CardActions>
                    </Card>
                </div>
                 ) 
          } );
    }


export default withStyles(styles)(Updates);

